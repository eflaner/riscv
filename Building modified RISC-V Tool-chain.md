[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Building a modified RISC-V Tool-chain 

While developing a new unstable extension in RISC-V, the tool-chain needs support of those extension, which could be bleeding edge prone to get broken, while using, than the stable release. Here we'll take a look in building the Bit manipulation Extension, which isn't officially supported by GCC, but under the development. 

### Prerequisites

1. Good Internet Connection
2. Patience (Nope - One coffee isn't enough)
3. At-least reserve 6 -8 GB in SSD (includes build)

### 2. Cloning
First create a directory (Possibly at places where permission isn't denied). I prefer to creating in **Documents** directory (You're free to choose else where). 
Create a new directory named **riscv-tools** this is where all the **RISC-V** tools will reside. The directory structure will be something like this.<br>

```text

Documents (your directory)
|-- riscv-tools
|   |-- riscv-bintuils-gdb
|   |-- riscv-gcc
|   |-- riscv-newlib
|   |-- riscv-opcodes
|   |-- riscv-openocd
|   |-- riscv-isa-sim
|   `-- riscv-pk
`-- riscv-install

```

<br>Open terminal and perform `cd Documents/riscv-tools`

#### Clone Repo's

Perform the operations below in the terminal <br>
`git clone --recursive https://github.com/riscv/riscv-binutils-gdb.git` <br>
`git clone --recursive https://github.com/riscv/riscv-gcc.git` <br>
`git clone --recursive https://github.com/riscv/riscv-newlib.git`<br>
`git clone --recursive https://github.com/riscv/riscv-opcodes.git`<br>
`git clone --recursive https://github.com/riscv/riscv-openocd.git`<br>
`git clone --recursive https://github.com/riscv/riscv-isa-sim`<br>
`git clone --recursive https://github.com/riscv/riscv-pk`

Optional Cloning
`git clone --recursive https://github.com/riscv/riscv-tests`

### 3. Checkout development branches 

This step takes example of building the **B** extension, you should take appropriate branch to checkout.

> :warning: Build might fail if the branches aren't selected appropriately.

Assuming the current pwd = *`~/Documents/riscv-tools`*

```shell
cd riscv-binutils-gdb && git checkout riscv-bitmanip

cd riscv-gcc && git checkout riscv-bitmanip

cd riscv-newlib && git checkout riscv-newlib-3.2.0

cd riscv-opcodes && git checkout riscv-bitmanip

cd riscv-openocd && git checkout riscv

cd riscv-isa-sim && git checkout riscv-bitmanip
```

### 4. Build and Installing Repo's

Now `Documents/riscv-tools` will have all the tools ready. Create a new directory  `Documents/riscv-install` where the build and installation will take place. Add the `riscv-install` to **`$RISCV`** path. For ease you can add to *`.bashrc`* and/or *`.profile`* for user-level access

> :warning: If a tool-chain already exists at the path, point a new path for new build

##### Adding to .bashrc 

`sudo nano ${HOME}/.bashrc`  In the opened file add the below two lines at the End of File

```bash
`export RISCV="Documents/riscv-install"`
`export PATH="${PATH}:${RISCV}/bin"`
```
Save and exit, perform `source ~/.bashrc` in terminal.

###### 3.1 Installing prerequisites

```shell
sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev
```

> :warning: Choose the soft/hard float modules & other architectural intricacies in the ***configure***

> :warning: For 64-bit artefacts, change 32 to 64 in the ***configure***


###### 3.2 riscv-binutils-gdb

```shell
cd ~/Documents/riscv-tools/riscv-binutils-gdb
rm -rf riscv-binutils-build && mkdir -p riscv-binutils-build && cd riscv-binutils-build
../configure --prefix="${RISCV}" --target=riscv32-unknown-elf
make -j4
make install
```

###### 3.3 riscv-gcc

```shell
cd ~/Documents/riscv-tools/riscv-gcc
rm -rf riscv-gcc-build && mkdir -p riscv-gcc-build && cd riscv-gcc-build
../configure --prefix=$RISCV --with-arch=rv32imacb --with-cmodel=medany --target=riscv32-unknown-elf --enable-languages=c --disable-libssp
make -j4
make install
```

###### 3.4 riscv-newlib

```shell
cd ~/Documents/riscv-tools/riscv-newlib
rm -rf riscv-newlib-build && mkdir -p riscv-newlib-build && cd riscv-newlib-build
../configure --prefix=$RISCV --with-arch=rv32imacb --with-cmodel=medany  --target=riscv32-unknown-elf
make -j4
make install
```

###### 3.5 Spike

```shell
cd Documents/riscv-tools/riscv-isa-sim
sudo apt-get install device-tree-compiler
mkdir -p build && cd build
 ../configure --prefix=$RISCV --with-isa=RV32IMAFDCB --enable-histogram --enable-commitlog
make 
make install 
```

> :warning: Choose the Debugging type (if needed) and below is sample of it

###### 3.6 OpenOCD

```shell
cd Documents/riscv-tools/riscv-openocd
mkdir -p build && cd build
../configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV
make 
make install 
```

###### 3.7 Proxy-Kernel

```shell
cd Documents/riscv-tools/riscv-pk
mkdir -p build && cd build 
 ../configure --prefix="${RISCV}" --host=riscv64-unknown-elf
make && make install
```


###### 3.8 check the path of installation 

`which riscv32-unknown-elf-gcc` - emits the path at which tool resides<br>
`which spike` - this should emit the path at which spike resides

###### 3.9 tests (Optional)

```shell
cd Documents/riscv-tools/riscv-tests
autoconf
./configure --prefix=$RISCV/riscv32-unknown-elf 
make && make install
```

Now you'v modified 32-bit RISC-V tool-chain installed you can run a **.c** program as below

Assuming we've a ***main.c*** file use below commands to perform

```shell
riscv32-unknown-elf-gcc main.c -o main.elf
riscv32-unknown-elf-objdump -d -S main.elf
timeout --foreground 1m spike -l pk main.elf
```

1st command performs compilation and generates executable<br>
2nd command shows RISC-V Instructions for the **main.c**<br>
3rd command simulates **main.elf** on RISC-V architecture

## Author

> **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
