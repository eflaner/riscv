[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Running Bare-metal C-program using RISC-V Tool-chain

## Generic Compilation & Simulation 

#### to run a c-program 

> riscv64-unknown-elf-gcc (Options as needed) (all src files) -o (output)
>
> `riscv64-unknown-elf-gcc demo.c -o demo.elf`

##### gcc Options (some of them) - [Complete Options](https://gcc.gnu.org/onlinedocs/gcc/Option-Summary.html)

- **-march** - Specify architecture - *rv64gc* - more info [here](https://www.sifive.com/blog/all-aboard-part-1-compiler-args)
- **-mabi** - Specify ABI - *ilp32 | ilp32d | lp64 | lp64d* - more info [here](https://wiki.gentoo.org/wiki/RISC-V_ABIs)
- **-mcmodel** -  Specify code model - medlow | medany - more info [here](https://www.sifive.com/blog/all-aboard-part-2-relocations)
- **-nostartfiles** - No initialization from Instruction - associated flags [here](https://stackoverflow.com/questions/10119520/g-static-initialization-and-nostdlib)
- **-T** - specify the location of linker file
- **-I** - specify the location of Header files
- **-ffreestanding** - No standard libraries 
- **-lm** - include library math - more info [here](https://stackoverflow.com/questions/11336477/gcc-will-not-properly-include-math-h)
- **-O0/1/2/3/s** - Optimizations - more info [here](https://gcc.gnu.org/onlinedocs/gcc-2.95.2/gcc_2.html#SEC10)

[Simplified explanation](https://github.com/riscv/riscv-toolchain-conventions/blob/master/README.mkd)

#### to view the assembly

> riscv64-unknown-elf-objdump (options) (executable) > (output)
>
> `riscv64-unknown-elf-objdump -d demo.elf > demo.disass`

##### objdump Options (some of them) - [Complete Options](https://sourceware.org/binutils/docs/binutils/objdump.html)

- **-d** - disassembles those sections that contains instructions
- **-D** - disassemble all ( All sections are displayed)
- **-l** - source line numbers corresponding to the object code
- **-S** - Display source code intermixed with disassembly
- **-M** numeric - use architectural register names such as x10

#### to simulate on spike

> spike (options) (executable) 2 > (output)
>
> `spike pk demo.elf 2> demo.dump`

if you need the code to be run without pk look at [baremetal section]()

##### spike options (some of them) - Complete Options @  `spike --help`

- **-l** - Generate a log of execution
- **-d** - Interactive debug mode
- **--isa=<name>** - RISC-V ISA String
- **pk** - use Proxy Kernel to run executable

#### Example of Generic Compilation

```shell
riscv64-unknown-elf-gcc fourierf.c fftmisc.c main.c -o fft.elf -lm
spike pk fft.elf 2 64
```

## Compiling for Bare-metal code - [source1](https://stackoverflow.com/a/31393890) & [source2](https://techroose.com/tech/spikeMulticore.html)

This compilation needs three files other than the source files 

1. [Boot code](./baremetal/init.s)
2. [System Calls](./baremetal/syscalls.c)
3. [linker file](./baremetal/link.ld)

The above files are stripped off versions from [here](https://github.com/riscv/riscv-tests/tree/master/benchmarks/common), with minimal instruction overhead yet supporting  global pointer, thread pointer, core allocated stack, trap - handling and graceful exit. If you're interested to build bigger programs on bare-metal with proper syscalls support, please use the files specified [here](https://github.com/riscv/riscv-tests/tree/master/benchmarks/common)

```shell
riscv64-unknown-elf-gcc -mcmodel=medany -nostartfiles ../baremetal/init.s ../baremetal/syscalls.c -T../baremetal/link.ld demo.c -o demo.elf
```

The GCC options specified above are mandatory, you might need more flags for a programs that need libraries of c

if you get any errors resolve it, with patience, using web-search 

simulating the bare-metal code on spike

```shell
spike -l demo.elf
```

for more fine tuned control from spike [look here](https://github.com/riscv/riscv-isa-sim#interactive-debug-mode)

## Author

> **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).
