extern volatile unsigned int tohost;

void __attribute__((noreturn)) tohost_exit(unsigned long  code)
{
  tohost = (code << 1) | 1;
  while (1);
}

int __attribute__((weak)) main(int argc, char** argv)
{
  // single-threaded programs override this function.
  return -1;
}

void _init()
{
  // char *inp[]={"fft","4","4096"};
  int ret = (int)main(0,0);
  tohost_exit(ret);
}