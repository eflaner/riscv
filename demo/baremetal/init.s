.section ".text.init"
.globl _begin
_begin:
    la t0, trap_entry
    csrw mtvec, t0
    .option push
    .option norelax
    la gp, __global_pointer$
    .option pop
    la  tp, _end + 63
    and tp, tp, -64
    csrr a0, mhartid
    sll a2, a0, 17
    add tp, tp, a2
    add sp, a0, 1
    sll sp, sp, 17
    add sp, sp, tp
    j _init

rept:
    li a0, 1
    sw a0, tohost, t0
    j rept

.align 2
trap_entry:
    li a0, 1337
    jal tohost_exit
    li t0, 0x00001800
    csrs mstatus, t0

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0
