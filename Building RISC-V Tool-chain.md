[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Building RISC-V Tool-chain (RV64)

### Prerequisites

1. Good Internet Connection
2. Patience (Nope - One coffee isn't enough)
3. At-least reserve 6 -8 GB in SSD (includes build)

### 2. Cloning
First create a directory (Possibly at places where permission isn't denied). I prefer to creating in **Documents** directory (You're free to choose else where). 
Create a new directory named **riscv-tools** this is where all the **RISC-V** tools will reside. The directory structure will be something like this.<br>

```text

Documents (your directory)
|-- riscv-tools
|   |-- riscv-gnu-toolchain
|   |-- riscv-isa-sim
|   |-- riscv-pk
|   |-- riscv-tests
|   `-- riscv-opcodes
`-- riscv-install

```

<br>Open terminal and perform `cd Documents/riscv-tools`

#### Clone Repo's

Perform the operations below in the terminal <br>
`git clone --recursive https://github.com/riscv/riscv-gnu-toolchain`
*<br>the above cloning takes good amount of time, you can `Ctrl+Shift+T` and perform the below cloning*

`git clone --recursive https://github.com/riscv/riscv-isa-sim`

`git clone --recursive https://github.com/riscv/riscv-opcodes`

`git clone --recursive https://github.com/riscv/riscv-pk`

Optional Cloning
`git clone --recursive https://github.com/riscv/riscv-pk`<br>
`git clone --recursive https://github.com/riscv/riscv-tests`

### 3. Build and Installing Repo's

Now `Documents/riscv-tools` will have all the tools ready. Create a new directory  `Documents/riscv-install` where the build and installation will take place. Add the `riscv-install` to **`$RISCV`** path. For ease you can add to *`.bashrc`* and/or *`.profile`* for user-level access

##### Adding to .bashrc 

`sudo nano ${HOME}/.bashrc`  In the opened file add the below two lines at the End of File

```bash
`export RISCV="Documents/riscv-install"`
`export PATH="${PATH}:${RISCV}/bin"`
```
Save and exit, perform `source ~/.bashrc` in terminal.

###### 3.2 gnu-tool-chain

```shell
cd Documents/riscv-tools/riscv-gnu-toolchain
sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev
./configure --prefix="${RISCV}" --with-arch=rv64imafdc --with-abi=lp64d --with-cmodel=medany --enable-multilib
make
```

###### 3.3 Proxy-Kernel

```shell
cd Documents/riscv-tools/riscv-pk
mkdir -p build && cd build 
 ../configure --prefix="${RISCV}" --host=riscv64-unknown-elf
make && make install
```

###### 3.4 Spike

```shell
cd Documents/riscv-tools/riscv-isa-sim
sudo apt-get install device-tree-compiler
mkdir -p build && cd build
 ../configure --prefix=$RISCV --with-isa=RV64IMAFDC --enable-histogram --enable-commitlog
make && make install 
```

###### 3.5 check the path of installation 

`which riscv64-unknown-elf-gcc` - emits the path at which tool resides<br>
`which spike` - this should emit the path at which spike resides

###### 3.6 tests (Optional)

```shell
cd Documents/riscv-tools/riscv-tests
autoconf
./configure --prefix=$RISCV/riscv64-unknown-elf 
make && make install
```

Now you've 64-bit RISC-V tool-chain installed you can run a **.c** program as below

Assuming we've a ***main.c*** file use below commands to perform

```shell
riscv64-unknown-elf-gcc main.c -o main.elf
riscv64-unknown-elf-objdump -d -S main.elf
timeout --foreground 1m spike -l pk main.elf
```

1st command performs compilation and generates executable<br>
2nd command shows RISC-V Instructions for the **main.c**<br>
3rd command simulates **main.elf** on RISC-V architecture

### 4 Note (To Build RV32)

Building RV32 needs a minor changes in the above flow

> 3.2 arch and abi

In section 3.3 replace the 3rd command with the below command

>  `../configure --prefix="${RISCV}" --host=riscv32-unknown-elf --enable-32bit && sed -i 's/\-m32//g' Makefile`

## Author

> **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).